// Gmsh project created on Tue Aug 14 22:02:37 2018
SetFactory("OpenCASCADE");
//+
Disk(1) = {0, 0, 0, 5, 5};
//+
Disk(2) = {0, 0, 0, 1, 1};
//+
BooleanDifference{ Surface{1}; Delete; }{ Surface{2}; }
//+
Physical Surface("charge", 1) = {2};
//+
Physical Surface("vacuum", 2) = {1};
//+
Physical Line("inner_boundary", 3) = {2};
//+
Physical Line("outer_boundary", 4) = {3};
//+
Transfinite Line {2} = 30 Using Progression 1;
//+
Transfinite Line {3} = 50 Using Progression 1;
